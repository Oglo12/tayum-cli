use clap::Parser;

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
#[command(propagate_version = true)]
pub struct Cli {
    #[clap(long, default_value = "2")]
    /// The minumum amount of cycles
    pub min: usize,

    #[clap(long, default_value = "9")]
    /// The maximum amount of cycles
    pub max: usize,
}

mod cli;

use clap::Parser;

fn main() {
    let args = cli::Cli::parse();

    let char_map = tayum::CharMap::default();
    let keys: Vec<char> = char_map.map.keys().into_iter().map(|x| *x).collect();
    let rules = tayum::WordRules::default();

    let word = tayum::generate_word(&rules, &char_map, &keys, args.min, args.max);

    println!("{word}");
}
